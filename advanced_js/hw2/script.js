const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві"
  }
];

const requireValues = [
  'author',
  'name',
  'price'
];

const root = document.querySelector('#root');
const template = document.querySelector('#listElem');

const list = createList(root);
addElems(list, books);

function validateBook(book) {
  let errors = [];
  requireValues.forEach((value) => {
    if (!book.hasOwnProperty(value)) {
      errors.push(value);
    }
  });

  if (errors.length) {
    throw new SyntaxError(errors.join(', '));
  }
}

function addElems(list, books) {
  books.forEach((book, index) => {
    try {
      validateBook(book);
      let elem = createElem(book);
      list.appendChild(elem);
    } catch (err) {
      console.error(`Ошибка вводных данных в книге №${index}. Свойств ${err.message} нет в книге`);
    }
  });
}

function createElem(book) {
  let clone = template.content.cloneNode(true);
  let p = clone.querySelectorAll("p");
  p.forEach((item) => {
    if (item.dataset.value) {
      item.innerText += book[item.dataset.value];
    }
  });
  return clone;
}

function createList(parent) {
  const ul = document.createElement('ul');
  parent.appendChild(ul);
  return ul;
}
