const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві"
  }
];

const requireValue = [
  'author', 'name', 'price'
];

const namesValue = new Map([
  ['author', 'Имя автора'],
  ['name', 'Название'],
  ['price', 'Цена']
]);

const root = document.getElementById('root');
const listUl = document.createElement('ul');
root.appendChild(listUl);
makeList(books);

function validateBook(book) {
  let errors = [];
  requireValue.forEach((value) => {
    if (!book.hasOwnProperty(value)) {
      errors.push(value);
    }
  });
  return errors;
}

function makeList(books) {
  books.forEach((book, index) => {
    let err = validateBook(book);
    if (err.length) {
      console.error(`Ошибка вводных данных в книге №${index}. Свойств ${err.join(', ')} нет в книге`);
    } else {
      createListElem(book);
    }
  });
}

function createListElem(book) {
  const listChild = document.createElement('li');
  listChild.innerHTML = 'Книга';
  let paramElems = '';
  for (const [key, value] of namesValue) {
    paramElems += `<p>${value}: ${book[key]}</p>`;
  }
  listChild.innerHTML += paramElems;
  listUl.appendChild(listChild);
}
