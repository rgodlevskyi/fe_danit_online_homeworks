const API_URL = 'https://ajax.test-danit.com/api/json';
const API_USERS = API_URL + '/users';
const API_POSTS = API_URL + '/posts';

function getUser(id) {
  return fetch(API_USERS + `/${id}`)
    .then(response => response.json());
}

function getPosts() {
  return fetch(API_POSTS)
    .then(response => response.json());
}

function deletePost(id) {
  return fetch(API_POSTS + `/${id}`, {
    method: 'DELETE'
  }).then(response => response.ok);
}

function addPost(post) {
  return fetch(API_POSTS, {
    method: 'POST',
    body: JSON.stringify(post)
  }).then(response => response.ok);
}

function updatePost(post, id) {
  return fetch(API_POSTS + `/${id}`, {
    method: 'PUT',
    body: JSON.stringify(post)
  }).then(response => response.ok);
}

export { getUser, getPosts, addPost, updatePost, deletePost };