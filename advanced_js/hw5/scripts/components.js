class Element {
  setTemplate(template) {
    this.view = template.content.cloneNode(true).firstElementChild;
  }

  click(callbackfn) {
    this.view.addEventListener('click', callbackfn);
  }

  render(slots) {
    this.view.dataset.id = this.id;

    let valuesNode = this.view.querySelectorAll('[data-value]');
    for (const valueNode of valuesNode) {
      let valueName = valueNode.dataset.value;
      if (this.data[valueName]) {
        valueNode.textContent = this.data[valueName];
      }
    }

    if (typeof slots === 'object' && slots !== undefined && slots !== null) {
      let slotsNode = this.view.querySelectorAll('[data-slot]');
      for (const slotNode of slotsNode) {
        let slotName = slotNode.dataset.slot;
        if (slots[slotName]) {
          slotNode.appendChild(slots[slotName].render());
        }
      }
    }

    return this.view;
  }

  removeView() {
    let parent = this.view.parentNode;
    parent.removeChild(this.view);
  }
}

class User extends Element {
  constructor({ name, username, email, id }) {
    super();
    this.data = {
      name: name,
      username: username,
      email: email
    };
    this.id = id;
  }
}

class Post extends Element {
  constructor({ title, body, id }) {
    super();
    this.data = {
      title: title,
      body: body
    };
    this.id = id;
  }
}

class Card extends Element {
  constructor(id) {
    super();
    this.id = id;
  }
}

class Button extends Element {
  constructor(id) {
    super();
    this.id = id;
  }
}

export { User, Post, Card, Button };