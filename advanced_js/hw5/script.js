import * as Api from "./scripts/api.js";
import * as Components from "./scripts/components.js";

async function loadTemplate(url) {
  return fetch(url)
    .then(response => response.text())
    .then(text => {
      let template = document.createElement('template');
      template.innerHTML = text;
      return template;
    });
}

const userTemplate = await loadTemplate('./templates/user.html');
const postTemplate = await loadTemplate('./templates/post.html');
const cardTemplate = await loadTemplate('./templates/card.html');
const buttonTemplate = await loadTemplate('./templates/button.html');

const root = document.querySelector('#root');

Api.getPosts().then(posts => {
  posts.forEach(post => {
    Api.getUser(post.userId).then(user => {
      let userComp = new Components.User(user);
      userComp.setTemplate(userTemplate);

      let postComp = new Components.Post(post);
      postComp.setTemplate(postTemplate);

      let buttonComp = new Components.Button(post.id);
      buttonComp.setTemplate(buttonTemplate);

      let cardComp = new Components.Card(post.id);
      cardComp.setTemplate(cardTemplate);
      cardComp.click(event => {
        if (event.target === buttonComp.view) {
          Api.deletePost(cardComp.id).then(status => {
            if (status) {
              cardComp.removeView();
            }
          })
        }
      });

      let cardNode = cardComp.render({
        user: userComp,
        post: postComp,
        delete: buttonComp
      });

      root.appendChild(cardNode);
    })
  });
})