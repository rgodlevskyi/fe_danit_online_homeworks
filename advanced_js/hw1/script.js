class Employee {
  constructor (name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  set name(setName) {
    this._name = value;
  }

  get age() {
    return this._age;
  }
  set age(setAge) {
    this._age = value;
  }

  get salary() {
    return this._salary;
  }
  set salary(setSalary) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get lang() {
    return this._lang;
  }
  set lang(setLang) {
    this._lang = value;
  }

  get salary() {
    return this._salary * 3;
  }
}

let dataEmployee = new Employee('David', '33', '4000');
let dataProgrammer = new Programmer('John', '27', '2000', 'JavaScript, React, SQL')

console.log(dataEmployee);
console.log(dataProgrammer);

console.log(dataProgrammer.salary)
