const API_URL = 'https://ajax.test-danit.com/api/swapi';
const root = document.querySelector('#root');
const listEpisodesTemplate = document.querySelector('#list');
const elementEpisodeTemplate = document.querySelector('#elementEpisode');
const listCharactersTemplate = document.querySelector('#list');
const elementCharacterTemplate = document.querySelector('#elementCharacter');
const loaderTemplate = document.querySelector('#loader');

function requestData(url) {
  return fetch(url).then(response => response.json());
}

function render() {
  requestData(API_URL + '/films').then(episodes => {
    episodes.sort((a, b) => a.episodeId - b.episodeId);

    const listEpisodes = copyTemplate(listEpisodesTemplate);

    episodes.forEach(episode => {
      const loader = copyTemplate(loaderTemplate);
      const elementEpisode = buildElementOfList(listEpisodes, elementEpisodeTemplate, episode);

      elementEpisode.appendChild(loader);
      root.appendChild(listEpisodes);

      getCharacters(episode.characters).then(characters => {
        elementEpisode.removeChild(loader);

        const listCharacters = copyTemplate(listCharactersTemplate);

        characters.forEach(character => {
          buildElementOfList(listCharacters, elementCharacterTemplate, character);
        });

        elementEpisode.appendChild(listCharacters);
      });
    });
  });
}

function getCharacters(urls) {
  return Promise.all(urls.map(requestData));
}

function copyTemplate(template) {
  const clone = template.content.cloneNode(true);
  return clone.firstElementChild;
}

function getPropertyNodes(parent) {
  return parent.querySelectorAll('[data-value]');
}

function buildElementOfList(listNode, template, data) {
  const node = copyTemplate(template);
  const propertyNodes = getPropertyNodes(node);

  for (const property of propertyNodes) {
    let value = property.dataset.value;
    if (value) {
      if (data[value]) {
        property.textContent += data[value];
      }
    }
  }

  return listNode.appendChild(node);
}

render();
