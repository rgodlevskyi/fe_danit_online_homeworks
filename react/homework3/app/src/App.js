import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";

import Storage from './Storage';
import Card from './Components/Card/Card';

import Catalog from './Components/Catalog/Catalog';
import Cart from './Components/Cart/Cart';
import Favorite from './Components/Favorite/Favorite';

function App() {
  const localStorage = new Storage();

  const [data, setData] = useState([]);
  const [favoriteCount, setFavoriteCount] = useState(localStorage.get('favorites').length);
  const [cartCount, setCartCount] = useState(localStorage.get('cart').length);

  useEffect(() => {
    fetch(`${process.env.PUBLIC_URL}/data.json`)
      .then(response => response.json())
      .then(result => setData(result));
  })

  let changeFavorite = (id) => {
    let favorites = localStorage.get('favorites');

    if (favorites.includes(id)) {
      favorites = favorites.filter(item => item !== id);
    } else {
      favorites.push(id);
    }

    setFavoriteCount(favorites.length);
    localStorage.set('favorites', favorites);

    return inFavorite(id);
  }

  let inFavorite = (id) => {
    let favorites = localStorage.get('favorites');
    return favorites.includes(id);
  }

  let changeCart = (id) => {
    let cart = localStorage.get('cart');

    if (cart.includes(id)) {
      cart = cart.filter(item => item !== id);
    } else {
      cart.push(id);
    }

    setCartCount(cart.length);
    localStorage.set('cart', cart);

    return inCart(id);
  }

  let inCart = (id) => {
    let cart = localStorage.get('cart');
    return cart.includes(id);
  }

  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Catalog</Link>
            </li>
            <li>
              <Link to="/cart">Cart {cartCount}</Link>
            </li>
            <li>
              <Link to="/favorite">Favorite {favoriteCount}</Link>
            </li>
          </ul>
        </nav>

        <Routes>
          <Route path="/" element={
            <Catalog data={data}>
              <Card
                onFavorite={changeFavorite}
                isFavorite={inFavorite}
                onCart={changeCart}
                isCart={inCart}
              />
            </Catalog>
          } />
          <Route path="/cart" element={
            <Cart data={data}>
              <Card
                onFavorite={changeFavorite}
                isFavorite={inFavorite}
                onCart={changeCart}
                isCart={inCart}
              />
            </Cart>
          } />
          <Route path="/favorite" element={
            <Favorite data={data}>
              <Card
                onFavorite={changeFavorite}
                isFavorite={inFavorite}
                onCart={changeCart}
                isCart={inCart}
              />
            </Favorite>
          } />
        </Routes>
      </div>
    </Router>
  )
}

export default App;
