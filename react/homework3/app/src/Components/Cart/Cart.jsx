import React, { useState, useEffect } from 'react';

import Storage from '../../Storage';
import List from '../List/List';
import Card from '../Card/Card';

function Cart(props) {
  const localStorage = new Storage();

  const [list, setList] = useState([]);

  useEffect(() => {
    let array = localStorage.get('cart');
    setList(props.data.filter(item => {
      for (const id of array) {
        if (id == item.article) {
          return true;
        }
      }

      return false;
    }))
  }, [props.data])

  return (
    <List data={list}>
      {props.children}
    </List>
  )
}

export default Cart;