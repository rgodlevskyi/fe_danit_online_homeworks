import React from "react";
import PropTypes from 'prop-types';

import './modal.scss';

function Modal(props) {
  return (
    <>
      {props.isOpen &&
        <>
          <div className="modal-bg" onClick={props.onClick}></div>
          <div className="modal">
            <div className="modal-title">
              <h3>{props.header}</h3>
              {props.closeButton && <button onClick={props.onClick} className="modal-btn"><strong>X</strong></button>}
            </div>
            <div className="modal-content">
              <p>{props.text}</p>
            </div>
            <div className="modal-footer">
              {props.actions}
            </div>
          </div>
        </>
      }
    </>
  );
}

Modal.propTypes = {
  isOpen: PropTypes.bool,
  onClick: PropTypes.func,
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.node,
}

Modal.defaultProps = {
  isOpen: false,
  header: 'Modal header',
  closeButton: true,
}

export default Modal;