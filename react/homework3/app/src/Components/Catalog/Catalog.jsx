import React from 'react';

import List from '../List/List';

function Catalog(props) {
  return (
    <List data={props.data}>
      {props.children}
    </List>
  )
}

export default Catalog;