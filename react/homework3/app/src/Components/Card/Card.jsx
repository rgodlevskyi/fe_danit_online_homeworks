import React, { useState } from "react";
import PropTypes from 'prop-types';

import Button from '../Button/Button';
import Modal from '../Modal/Modal';

import './card.scss';

function Card(props) {
  const [isFavorite, setFavorite] = useState(props.isFavorite(props.data.article));
  const [isCart, setCart] = useState(props.isCart(props.data.article));

  const [isOpenAddToCartModal, setAddToCartModal] = useState(false);
  const [isOpenRemoveFromCartModal, setRemoveCartModal] = useState(false);
  const [isOpenAddToFavoriteModal, setAddToFavoriteModal] = useState(false);
  const [isOpenRemoveFromFavoriteModal, setRemoveFavoriteModal] = useState(false);

  let closeModals = () => {
    setAddToCartModal(false);
    setRemoveCartModal(false);
    setAddToFavoriteModal(false);
    setRemoveFavoriteModal(false);
  }

  let displayAddToCartModal = () => {
    setAddToCartModal(true);
  }

  let displayRemoveFromCartModal = () => {
    setRemoveCartModal(true);
  }

  let displayAddToFavoriteModal = () => {
    setAddToFavoriteModal(true);
  }

  let displayRemoveFromFavoriteModal = () => {
    setRemoveFavoriteModal(true);
  }

  let { name, url, article, color, price } = props.data;
  return (
    <>
      <div className="card">
        <div className="img-size">
          <img src={url} alt="" />
        </div>
        <p>Color: {color}</p>
        <h2>{name}</h2>
        <p>Article: {article}</p>
        <h2>Price: {price}</h2>
        <div className="buttons">
        <Button
          text={isCart ? "Remove from Cart" : "Add to Cart"}
          onClick={isCart ? displayRemoveFromCartModal : displayAddToCartModal}
          backgroundColor={isCart ? "red" : "green"}
          className="addToCard"
        />
        <Button
          text={isFavorite ? "Remove from Favorite" : "Add to Favorite"}
          onClick={isFavorite ? displayRemoveFromFavoriteModal : displayAddToFavoriteModal}
          backgroundColor={isFavorite ? "blue" : "gray"}
          className="addToFavorite"
        />
        </div>
      </div>
      <Modal
        header='Add item to Cart?'
        isOpen={isOpenAddToCartModal}
        onClick={closeModals}
        closeButton={true}
        actions={
          <>
            <Button
              text='Add'
              onClick={() => {
                setCart(props.onCart(article));
                closeModals();
              }}
              backgroundColor="green"
            />
            <Button
              text='Close'
              onClick={closeModals}
              backgroundColor="blue"
            />
          </>
        }
      />
      <Modal
        header='Remove item from Cart?'
        isOpen={isOpenRemoveFromCartModal}
        onClick={closeModals}
        closeButton={true}
        actions={
          <>
            <Button
              text='Remove'
              onClick={() => {
                setCart(props.onCart(article));
                closeModals();
              }}
              backgroundColor="red"
            />
            <Button
              text='Close'
              onClick={closeModals}
              backgroundColor="blue"
            />
          </>
        }
      />
      <Modal
        header='Add item to Favorite?'
        isOpen={isOpenAddToFavoriteModal}
        onClick={closeModals}
        closeButton={true}
        actions={
          <>
            <Button
              text='Add'
              onClick={() => {
                setFavorite(props.onFavorite(article));
                closeModals();
              }}
              backgroundColor="green"
            />
            <Button
              text='Close'
              onClick={closeModals}
              backgroundColor="blue"
            />
          </>
        }
      />
      <Modal
        header='Remove item from Favorite?'
        isOpen={isOpenRemoveFromFavoriteModal}
        onClick={closeModals}
        closeButton={true}
        actions={
          <>
            <Button
              text='Remove'
              onClick={() => {
                setFavorite(props.onFavorite(article));
                closeModals();
              }}
              backgroundColor="red"
            />
            <Button
              text='Close'
              onClick={closeModals}
              backgroundColor="blue"
            />
          </>
        }
      />
    </>
  )
}

Card.propTypes = {
  data: PropTypes.object,
  onCart: PropTypes.func,
  isCart: PropTypes.func,
  onFavorite: PropTypes.func,
  isFavorite: PropTypes.func,
}

export default Card;