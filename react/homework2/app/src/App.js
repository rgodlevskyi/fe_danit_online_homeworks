import React from 'react';

import Storage from './Storage';
import Button from './Components/Button/Button';
import Modal from './Components/Modal/Modal';
import Card from './Components/Card/Card';
import List from './Components/List/List';

class App extends React.Component {
  state = {
    data: [],
    favoriteCount: 0,
    cartCount: 0,
    modal: <></>
  }

  constructor(props) {
    super(props);
    this.localStorage = new Storage();
  }

  addToFavorite = (id) => {
    let favorites = this.localStorage.get('favorites');

    id = String(id);

    if (favorites.includes(id)) {
      favorites = favorites.filter(item => item !== id);
    } else {
      favorites.push(id);
    }

    this.setState({ favoriteCount: favorites.length });
    this.localStorage.set('favorites', favorites);

    return this.isFavorite(id);
  }

  isFavorite = (id) => {
    let favorites = this.localStorage.get('favorites');
    return favorites.includes(String(id));
  }

  addToCart = (id) => {
    let cart = this.localStorage.get('cart');

    if (!cart.includes(String(id))) {
      cart.push(String(id));
      this.setState({ cartCount: cart.length });
      this.localStorage.set('cart', cart);
    }

    this.closeModal();
  }

  displayModal = (id) => {
    this.setState({
      modal: React.createElement(Modal, {
        header: 'Add to Cart?',
        isOpen: true,
        closeButton: true,
        onClick: this.closeModal,
        actions:
          <>
            <Button
              text="Add"
              onClick={() => this.addToCart(id)}
              backgroundColor="green"
            />
            <Button
              text="Close"
              onClick={this.closeModal}
              backgroundColor="red"
            />
          </>
      })
    })
  }

  closeModal = () => {
    this.setState({
      modal: <></>
    })
  }

  componentDidMount() {
    fetch(`${process.env.PUBLIC_URL}/data.json`)
      .then(response => response.json())
      .then(result => this.setState({ data: result }));

    this.setState({
      favoriteCount: this.localStorage.get('favorites').length,
      cartCount: this.localStorage.get('cart').length,
    });
  }

  render() {
    return <>
      <div>
        <p>Cart: {this.state.cartCount}</p>
        <p>Favorite: {this.state.favoriteCount}</p>
      </div>
      <List
        data={this.state.data}
      >
        <Card
          onFavorite={this.addToFavorite}
          isFavorite={this.isFavorite}
          onCart={this.displayModal}
        />
      </List>
      {this.state.modal}
    </>
  }
}

export default App;
