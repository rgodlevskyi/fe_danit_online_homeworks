import React from "react";
import PropTypes from 'prop-types';

import './list.scss';

class List extends React.Component {
    render() {
        let list = this.props.data.map(item =>
            React.cloneElement(this.props.children, {
                key: item.article,
                data: item,
            })
        );

        return (
            <div className="container">
                {list}
            </div>
        )
    }
}

List.propTypes = {
    data: PropTypes.array,
    children: PropTypes.node,
}

List.defaultProps = {
    data: [],
}

export default List;