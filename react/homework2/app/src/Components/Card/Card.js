import React from "react";
import PropTypes from 'prop-types';

import Button from '../Button/Button'

import './card.scss';

class Card extends React.Component {
  state = {
    isFavorite: false,
  }

  componentDidMount() {
    let isFavorite = this.props.isFavorite(this.props.data.article);
    this.setState({isFavorite: isFavorite});
  }

  render() {
    let { name, url, article, color, price } = this.props.data;

    return (
      <div className="card">
        <div className="img-size">
          <img src={url} alt="" />
        </div>
        <p>Color: {color}</p>
        <h2>{name}</h2>
        <p>Article: {article}</p>
        <h2>Price: {price}</h2>
        <div className="buttons">
          <Button
            text="Add to Cart"
            onClick={() => this.props.onCart(article)}
            className="addToCard"
          />
          <Button
            text="Add to Favorite"
            onClick={() => this.setState({isFavorite: this.props.onFavorite(article)})}
            backgroundColor={this.state.isFavorite ? 'green' : 'gray'}
            className="addToFavorite"
          />
        </div>
      </div>
    )
  }
}

Card.propTypes = {
  data: PropTypes.object,
  onCart: PropTypes.func,
  onFavorite: PropTypes.func,
  isFavorite: PropTypes.func,
}

export default Card;