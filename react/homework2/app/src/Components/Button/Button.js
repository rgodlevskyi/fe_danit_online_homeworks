import React from "react";
import PropTypes from 'prop-types';

class Button extends React.Component {
  render() {
    let { onClick, backgroundColor, text, className } = this.props;
    return (
      <button
        className={className}
        onClick={onClick}
        style={{ backgroundColor: backgroundColor }}
      >
        {text}
      </button>
    )
  }
}

Button.propTypes = {
  onClick: PropTypes.func,
  style: PropTypes.string,
  text: PropTypes.string,
  className: PropTypes.string
}

Button.defaultProps = {
  style: 'white',
  text: 'Button'
}

export default Button;