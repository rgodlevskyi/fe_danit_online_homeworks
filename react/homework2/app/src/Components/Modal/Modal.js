import React from "react";
import PropTypes from 'prop-types';

import './modal.scss';

class Modal extends React.Component {
  render() {
    return (
      <>
        {this.props.isOpen &&
          <>
            <div className="modal-bg" onClick={this.props.onClick}></div>
            <div className="modal">
              <div className="modal-title">
                <h2>{this.props.header}</h2>
                {this.props.closeButton && <button onClick={this.props.onClick} className="modal-btn"><strong>X</strong></button>}
              </div>
              <div className="modal-footer">
                {this.props.actions}
              </div>
            </div>
          </>
        }
      </>
    );
  }
}

Modal.propTypes = {
  isOpen: PropTypes.bool,
  onClick: PropTypes.func,
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.node,
}

Modal.defaultProps = {
  isOpen: false,
  header: 'Modal header',
  closeButton: true,
}

export default Modal;