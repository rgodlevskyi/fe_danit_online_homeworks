class Storage {
    constructor() {
        this.storage = window.localStorage;
    }

    get(item) {
        let str = this.storage.getItem(item) || '';
        let array = [];
        if (str.length !== 0) {
            array = str.split(',');
        }

        array.forEach((item, index, array) => {
            array[index] = Number(item);
        });

        return array;
    }

    set(item, data) {
        this.storage.setItem(item, data);
    }
}

export default Storage;