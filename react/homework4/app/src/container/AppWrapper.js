import {connect} from 'react-redux';
import addData from '../store/actions/addData';
import addCartCount from '../store/actions/addCartCount';
import addFavoriteCount from '../store/actions/addFavoriteCount';
import App from '../components/App';

const mapStateToProps = state => {
    return {
        data: state.data,
        cartCount: state.cartCount,
        favoriteCount: state.favorite.length,
        modal: state.modal,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setData: data => {
            dispatch(addData(data));
        },
        setCartCount: count => {
            dispatch(addCartCount(count));
        },
        setFavoriteCount: count => {
            dispatch(addFavoriteCount(count));
        }
    }
}

const AppWrapper = connect(
    mapStateToProps,
    mapDispatchToProps
)(App);

export default AppWrapper;