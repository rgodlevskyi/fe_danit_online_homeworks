import {connect} from 'react-redux';

import openModal from '../store/actions/openModal';
import closeModal from '../store/actions/closeModal';

import addToFavorite from '../store/actions/addToFavorite';
import removeFromFavorite from '../store/actions/removeFromFavorite';

import Card from '../components/Card';

const mapStateToProps = state => {
    return {
        favorite: state.favorite,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        openModal: modal => {
            dispatch(openModal(modal));
        },
        closeModal: modal => {
            dispatch(closeModal(modal));
        },
        addToFavorite: id => {
            dispatch(addToFavorite(id));
        },
        removeFromFavorite: id => {
            dispatch(removeFromFavorite(id));
        }
    }
}

const CardWrapper = connect(
    mapStateToProps,
    mapDispatchToProps
)(Card);

export default CardWrapper;