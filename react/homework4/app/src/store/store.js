import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import localStorage from './middlewares/localStorage';

import rootReducer from './reducers/rootReduser';

const store = createStore(rootReducer, compose(applyMiddleware(thunk), applyMiddleware(localStorage)));

export default store;