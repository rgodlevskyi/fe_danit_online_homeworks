import ADD_DATA from "../types/ADD_DATA";

const data = (state = [], action) => {
    switch(action.type) {
        case ADD_DATA: return action.data;

        default: return state;
    }
}

export default data;