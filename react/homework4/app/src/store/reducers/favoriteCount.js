const favoriteCount = (state = 0, action) => {
    switch(action.type) {
        case 'addFavoriteCount': return action.favoriteCount;

        default: return state;
    }
}

export default favoriteCount;