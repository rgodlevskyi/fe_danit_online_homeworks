import Storage from "../../Storage";

const initialState = new Storage().get('favorites');

const favorite = (state = initialState, action) => {
    switch(action.type) {
        case 'addToFavorite': return [...state, action.id];
        case 'removeFromFavorite': return state.filter(item => item !== action.id);
        case 'isFavorite': return state.find(item => item === action.id) ? true : false;

        default: return state;
    }
}

export default favorite;