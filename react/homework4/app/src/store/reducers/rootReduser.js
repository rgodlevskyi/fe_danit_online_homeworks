import { combineReducers } from 'redux';

import data from "./data";
import cartCount from './cartCount';
import favoriteCount from './favoriteCount';
import modal from './modal';
import favorite from './favorite';

const rootReducer = combineReducers({
    data,
    cartCount,
    favoriteCount,
    modal,
    favorite,
})

export default rootReducer;
