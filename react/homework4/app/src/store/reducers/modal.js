const modal = (state = {
    header: '',
    text: '',
    isOpen: false,
    closeButton: false,
    onClick: () => {},
    actions: <></>
}, action) => {
    switch(action.type) {
        case 'openModal': return {
            header: action.modal.header,
            text: action.modal.text,
            closeButton: action.modal.closeButton,
            onClick: action.modal.onClick,
            actions: action.modal.actions,
            isOpen: true,
        };
        case 'closeModal': return {
            header: '',
            text: '',
            closeButton: false,
            onClick: () => {},
            actions: <></>,
            isOpen: false,
        };

        default: return state;
    }
}

export default modal;