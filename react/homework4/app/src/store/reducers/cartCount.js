const cartCount = (state = 0, action) => {
    switch(action.type) {
        case 'addCartCount': return action.cartCount;

        default: return state;
    }
}

export default cartCount;