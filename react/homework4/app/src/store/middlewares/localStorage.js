import Storage from "../../Storage";

const localStorage = (store) => (next) => (action) => {
    const storage = new Storage();

    switch(action.type) {
        case 'addToFavorite': {
            storage.set('favorites', [...store.getState().favorite, action.id]);
            break;
        };
        case 'removeFromFavorite': {
            storage.set('favorites', store.getState().favorite.filter(item => item !== action.id));
            break;
        }

        default: {}
    }

    return next(action);
};

export default localStorage