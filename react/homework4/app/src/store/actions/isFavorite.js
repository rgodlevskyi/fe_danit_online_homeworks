function isFavorite() {
    return { 
        type: 'isFavorite'
    };
}

export default isFavorite;