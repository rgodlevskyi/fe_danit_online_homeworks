import ADD_DATA from "../types/ADD_DATA";

function addData(value) {
    return { 
        type: ADD_DATA,
        data: value
    };
}

export default addData;