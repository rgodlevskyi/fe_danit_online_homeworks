function removeFromFavorite(value) {
    return { 
        type: 'removeFromFavorite',
        id: value
    };
}

export default removeFromFavorite;