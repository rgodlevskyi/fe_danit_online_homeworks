function addCartCount(value) {
    return { 
        type: 'addCartCount',
        cartCount: value
    };
}

export default addCartCount;