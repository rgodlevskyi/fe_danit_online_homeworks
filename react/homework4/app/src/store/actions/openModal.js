function openModal(value) {
    return { 
        type: 'openModal',
        modal: value
    };
}

export default openModal;