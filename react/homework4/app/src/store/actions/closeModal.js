function closeModal(value) {
    return { 
        type: 'closeModal',
        modal: value
    };
}

export default closeModal;