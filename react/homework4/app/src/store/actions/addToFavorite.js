function addToFavorite(value) {
    return { 
        type: 'addToFavorite',
        id: value
    };
}

export default addToFavorite;