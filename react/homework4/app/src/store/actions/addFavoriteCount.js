function addFavoriteCount(value) {
    return { 
        type: 'addFavoriteCount',
        favoriteCount: value
    };
}

export default addFavoriteCount;