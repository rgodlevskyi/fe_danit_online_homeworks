import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";

import Storage from '../Storage';
import Modal from './Modal';

import Catalog from './Catalog';
import Cart from './Cart';
import Favorite from './Favorite';

import CardWrapper from '../container/CardWrapper';

function App(props) {
  const localStorage = new Storage();

  useEffect(() => {
    fetch(`${process.env.PUBLIC_URL}/data.json`)
      .then(response => response.json())
      .then(result => props.setData(result));

    props.setCartCount(localStorage.get('cart').length);
    // props.setFavoriteCount(localStorage.get('favorites').length);
  }, [])

  let changeFavorite = (id) => {
    let favorites = localStorage.get('favorites');

    if (favorites.includes(id)) {
      favorites = favorites.filter(item => item !== id);
    } else {
      favorites.push(id);
    }

    // props.setFavoriteCount(favorites.length);
    // localStorage.set('favorites', favorites);

    return inFavorite(id);
  }

  let inFavorite = (id) => {
    let favorites = localStorage.get('favorites');
    return favorites.includes(id);
  }

  let changeCart = (id) => {
    let cart = localStorage.get('cart');

    if (cart.includes(id)) {
      cart = cart.filter(item => item !== id);
    } else {
      cart.push(id);
    }

    props.setCartCount(cart.length);
    localStorage.set('cart', cart);

    return inCart(id);
  }

  let inCart = (id) => {
    let cart = localStorage.get('cart');
    return cart.includes(id);
  }

  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Catalog</Link>
            </li>
            <li>
              <Link to="/cart">Cart {props.cartCount}</Link>
            </li>
            <li>
              <Link to="/favorite">Favorite {props.favoriteCount}</Link>
            </li>
          </ul>
        </nav>

        <Modal
          header={props.modal.header}
          text={props.modal.text}
          closeButton={props.modal.closeButton}
          isOpen={props.modal.isOpen}
          onClick={props.modal.onClick}
          actions={props.modal.actions}
        />

        <Routes>
          <Route path="/" element={
            <Catalog data={props.data}>
              <CardWrapper
                onFavorite={changeFavorite}
                isFavorite={inFavorite}
                onCart={changeCart}
                isCart={inCart}
              />
            </Catalog>
          } />
          <Route path="/cart" element={
            <Cart data={props.data}>
              <CardWrapper
                onFavorite={changeFavorite}
                isFavorite={inFavorite}
                onCart={changeCart}
                isCart={inCart}
              />
            </Cart>
          } />
          <Route path="/favorite" element={
            <Favorite data={props.data}>
              <CardWrapper
                onFavorite={changeFavorite}
                isFavorite={inFavorite}
                onCart={changeCart}
                isCart={inCart}
              />
            </Favorite>
          } />
        </Routes>
      </div>
    </Router>
  )
}

export default App;
