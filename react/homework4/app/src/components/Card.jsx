import React, { useState } from "react";
import PropTypes from 'prop-types';

import Button from './Button';

function Card(props) {
  const [isCart, setCart] = useState(props.isCart(props.data.article));

  let isFavorite = () => {
    return props.favorite.find(item => item === article) ? true : false;
  }

  let displayAddToCartModal = () => {
    props.openModal({
      header: 'Add item to cart?',
      closeButton: true,
      onClick: props.closeModal,
      actions: <>
        <Button
          text='Add'
          onClick={() => {
            setCart(props.onCart(article));
            props.closeModal();
          }}
          backgroundColor="green"
          className="addToCard"
        />
        <Button
          text='Close'
          onClick={props.closeModal}
          backgroundColor="blue"
          className="addToFavorite"
        />
      </>
    })
  }

  let displayRemoveFromCartModal = () => {
    props.openModal({
      header: 'Remove item from cart?',
      closeButton: true,
      onClick: props.closeModal,
      actions: <>
        <Button
          text='Remove'
          onClick={() => {
            setCart(props.onCart(article));
            props.closeModal();
          }}
          backgroundColor="red"
        />
        <Button
          text='Close'
          onClick={props.closeModal}
          backgroundColor="blue"
        />
      </>
    })
  }

  let displayAddToFavoriteModal = () => {
    props.openModal({
      header: 'Add item to favorite?',
      closeButton: true,
      onClick: props.closeModal,
      actions: <>
        <Button
          text='Add'
          onClick={() => {
            // setFavorite(props.onFavorite(article));
            props.addToFavorite(article);
            props.closeModal();
          }}
          backgroundColor="green"
        />
        <Button
          text='Close'
          onClick={props.closeModal}
          backgroundColor="blue"
        />
      </>
    })
  }

  let displayRemoveFromFavoriteModal = () => {
    props.openModal({
      header: 'Remove item from favorite?',
      closeButton: true,
      onClick: props.closeModal,
      actions: <>
        <Button
          text='Remove'
          onClick={() => {
            props.removeFromFavorite(article);
            props.closeModal();
          }}
          backgroundColor="red"
        />
        <Button
          text='Close'
          onClick={props.closeModal}
          backgroundColor="blue"
        />
      </>
    })
  }

  let { name, url, article, color, price } = props.data;
  return (
    <>
      <div className="card">
        <div className="img-size">
          <img src={url} alt="" />
        </div>
        <p>Color: {color}</p>
        <h2>{name}</h2>
        <p>Article: {article}</p>
        <h2>Price: {price}</h2>
        <div className="buttons">
        <Button
          text={isCart ? "Remove from Cart" : "Add to Cart"}
          onClick={isCart ? displayRemoveFromCartModal : displayAddToCartModal}
          backgroundColor={isCart ? "red" : "green"}
          className="addToCard"
        />
        <Button
          text={isFavorite() ? "Remove from Favorite" : "Add to Favorite"}
          onClick={isFavorite() ? displayRemoveFromFavoriteModal : displayAddToFavoriteModal}
          backgroundColor={isFavorite() ? "blue" : "gray"}
          className="addToFavorite"
        />
        </div>
      </div>
    </>
  )
}

Card.propTypes = {
  data: PropTypes.object,
  onCart: PropTypes.func,
  isCart: PropTypes.func,
  onFavorite: PropTypes.func,
  isFavorite: PropTypes.func,
}

export default Card;