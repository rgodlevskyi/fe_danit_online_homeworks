import React from 'react';

import List from './List';

function Catalog(props) {
  return (
    <List data={props.data}>
      {props.children}
    </List>
  )
}

export default Catalog;