import React, {useState, useEffect} from "react";

import List from "./List";
import Storage from "../Storage";

export default function Favorite(props) {
  const localStorage = new Storage();

  const [list, setList] = useState([]);

  useEffect(() => {
    let array = localStorage.get('favorites');
    setList(props.data.filter(item => {
      for (const id of array) {
        if (id === item.article) {
          return true;
        }
      }

      return false;
    }))
  }, [props.data])

  return (
    <List data={list}>
      {props.children}
    </List>
  )
}