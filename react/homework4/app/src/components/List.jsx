import React from "react";
import PropTypes from 'prop-types';

function List(props) {
    let list = props.data.map(item =>
        React.cloneElement(props.children, {
            key: item.article,
            data: item,
        })
    );

    return (
        <div className="container">
            {list}
        </div>
    )
}

List.propTypes = {
    data: PropTypes.array,
    children: PropTypes.node,
}

List.defaultProps = {
    data: [],
}

export default List;