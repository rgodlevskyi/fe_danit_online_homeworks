import React from 'react';

import Button from './Components/Button/Button';
import Modal from './Components/Modal/Modal';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.toggleFirst = this.toggleFirst.bind(this);
        this.toggleSecond = this.toggleSecond.bind(this);
        this.state = {displayFirst: false, displaySecond: false};
    }

    toggleFirst() {
        this.setState({displayFirst: !this.displayFirst});
    }

    toggleSecond() {
        this.setState({displaySecond: !this.displaySecond});
    }

    closeModal() {
        this.setState({displayFirst: false, displaySecond: false});
    }

    render() {
        return <>
            <Button
                text="Open first modal"
                onClick={this.toggleFirst}
                backgroundColor="red"
                color="white"
            />
            <Button
            text="Open second modal"
            onClick={this.toggleSecond}
            backgroundColor="blue"
            color="white"
            />
            <Modal
                header="First Modal Header"
                isOpen={this.state.displayFirst}
                closeButton={true}
                onClick={() => this.closeModal()}
                text="First Modal text"
                className="first-modal"
                actions={
                    <Button
                        text="Close"
                        onClick={()=>this.closeModal()}
                        backgroundColor="gray"
                        color="white"
                    />
                }
            />
            <Modal
                header="Second Modal Header"
                isOpen={this.state.displaySecond}
                closeButton={ true }
                onClick={() => this.closeModal()}
                text="Second Modal text"
                className="second-modal"
                actions={
                    <Button
                        text="Close"
                        onClick={()=>this.closeModal()}
                        backgroundColor="gray"
                        color="white"
                    />
                }
            />
        </>
    }
}

export default App;
