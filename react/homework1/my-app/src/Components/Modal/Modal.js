import React from "react";

import './modal.scss';

class Modal extends React.Component {
  render() {
    return (
      <>
        {this.props.isOpen &&
          <>
            <div className="modal-bg" onClick={this.props.onClick}></div>
            <div className={this.props.className + ' modal'}>
              <div className="modal-title">
                <h1>{this.props.header}</h1>
                {this.props.closeButton && <button onClick={this.props.onClick}><strong>X</strong></button>}
              </div>
              <div className="modal-content">
                <p>{this.props.text}</p>
              </div>
              <div className="modal-footer">
                {this.props.actions}
              </div>
            </div>
          </>
        }
      </>
    );
  }
}

export default Modal;